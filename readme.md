# 作业要求

## 1 Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 每一种 case 都必须至少包含一个独立的测试。
* 所有的测试必须都成功。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 2 请书写测试来验证如下的 case

如果你已经忘记了之前的内容你可以观察 test/java/com/twuc/webApp/simplest 目录下的范例测试。它可以帮助你回忆起来。以下的所有测试请在 **test/java/com/twuc/webApp/yourTurn** 下书写。

### 2.1 `AnnotationConfigApplicationContext` 的使用

* 请书写测试，使用 `AnnotationConfigApplicationContext` 创建一个没有依赖的对象 `WithoutDependency`。请确保对象创建成功，且类型也符合预期。
* 请书写测试，使用 `AnnotationConfigApplicationContext` 创建一个有依赖的对象（`WithDependency` 依赖 `Dependent`）。请确保创建之后对象和对象的依赖都创建成功了。
* 请书写测试，如果希望创建的对象类型 `OutOfScanningScope` 位于 `AnnotationConfigApplicationContext` 的扫描范围之外会出现什么情况。
* 如果我将类型 `InterfaceImpl` 标记为 `@Component` 但是该类型实现了接口 `Interface` 那么 `getBean` 的时候可以使用 `Interface.class` 进行对象创建么？写测试证明这种情况。
* 请书写测试。有一个对象 `SimpleObject` 实现了接口 `SimpleInterface`，而 `SimpleObject` 还依赖于 `SimpleDependent` 类型，`SimpleDependent` 没有带参数的构造函数，但是有一个 `setName` 方法。同时可以通过 `getName` 取得 `name`。我希望当我调用 `context.getBean(SimpleInterface.class)` 的时候返回的对象中的 `SimpleDependent` 对象的 `name` 为 `"O_o"`。用 `@Configuration` 和 `@Bean` 实现上述功能。（可以参考：https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html#beans-java-bean-annotation *Section 1.12.3*）

### 2.2 `AnnotationConfigApplicationContext` 其他用例

* 请书写测试，假设一个类 `MultipleConstructor` 有多个构造函数。其中一个依赖于 `Dependent` 另一个需要传入一个 `String`。我们需要告知 `context` 使用第一个构造函数进行依赖的表达，那么应该如何实现。写测试真实这个过程。（可以参考：https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html#beans-autowired-annotation *Section 1.9.2*）
* 假设一个类型 `WithAutowiredMethod` 依赖于 `Dependent`。同时这个类还有一个方法叫做 `initialize` 这个方法接收一个 `AnotherDependent` 类型，且这个方法还被标记为了 `@Autowired`。那么在 `context.getBean(WithAutowiredMethod.class)` 之后，其构造函数和 `initialize` 方法是否都被调用？调用的顺序如何？各次调用的参数值是否为 `null`？写测试证明这个过程。（可以参考：https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html#beans-autowired-annotation *Section 1.9.2*）
* 假设一个接口 `InterfaceWithMultipleImpls` 拥有多个实现：`ImplementationA`, `ImplementationB`, `ImplementationC`。那么能否一次性的使用 `context` 创建一个数组，其中包含上述三个实现的一个实例。（可以参考：https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/context/annotation/AnnotationConfigApplicationContext.html 是时候发挥你的英语水平了，猜一猜你应该使用哪个方法？）