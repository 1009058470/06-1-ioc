package com.twuc.webApp.entity;

public class SimpleDependent {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
