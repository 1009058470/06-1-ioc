package com.twuc.webApp.entity;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {

    private String name;

    private Dependent dependent;

    public MultipleConstructor(String name) {
        this.name = name;
    }

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public String getName() {
        return name;
    }

    public Dependent getDependent() {
        return dependent;
    }
}
