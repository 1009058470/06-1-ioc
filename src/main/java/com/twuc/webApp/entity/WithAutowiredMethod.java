package com.twuc.webApp.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {

    private Dependent dependent;
    private AnotherDependent anotherDependent;

    private MyLogger myLogger;


    public WithAutowiredMethod(Dependent dependent, MyLogger myLogger) {
        this.dependent = dependent;
        this.myLogger = myLogger;
        this.myLogger.getLogger().add("this is dependent inject");
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent){
        this.myLogger.getLogger().add("this is another dependent inject");
        this.anotherDependent = anotherDependent;
    }

    public Dependent getDependent() {
        return dependent;
    }


    public MyLogger getMyLogger() {
        return myLogger;
    }
}
